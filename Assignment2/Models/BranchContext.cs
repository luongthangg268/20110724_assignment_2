﻿using Microsoft.EntityFrameworkCore;

namespace Assignment2.Models
{
    public class BranchContext : DbContext
    {
        public BranchContext(DbContextOptions<BranchContext> options)
        : base(options)
        {
        }

        public DbSet<Branch> Branchs { get; set; } = null!;
    }
}
